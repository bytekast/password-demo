[![Build Status](https://travis-ci.org/bytekast/password-demo.svg)](https://travis-ci.org/bytekast/password-demo)

[![Coverage Status](https://coveralls.io/repos/bytekast/password-demo/badge.svg?branch=master&service=github)](https://coveralls.io/github/bytekast/password-demo?branch=master)

password-demo
========================

Make sure you have [Maven](https://maven.apache.org/) installed.

Run Tests and Generate Coverage Reports: `mvn clean test jacoco:report`

Build Artifacts: `mvn package`

Run Locally: `mvn spring-boot:run`

The default address is: http://localhost:8080/validate/${password}