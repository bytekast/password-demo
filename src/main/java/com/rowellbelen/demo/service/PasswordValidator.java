package com.rowellbelen.demo.service;


import com.rowellbelen.demo.validator.Validator;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Rowell Belen
 */
@RestController
public class PasswordValidator {

  // Inject all spring-managed beans that implement Validator
  // If AnnotationConfig (@Autowired) is unavailable, we could add a constructor/setter for IoC
  @Autowired
  private Collection<Validator> validators;

  @RequestMapping(value = {"/validate/{password}"})
  public boolean isValid(@PathVariable(value = "password") final String password) {

    for (Validator validator : validators) {
      if (!validator.isValid(password)) {
        return false;
      }
    }

    return true;
  }
}
