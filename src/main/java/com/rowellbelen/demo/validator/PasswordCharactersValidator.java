package com.rowellbelen.demo.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

/**
 * @author Rowell Belen
 */
@Component
public class PasswordCharactersValidator implements Validator {

  // This could be made configurable via a property, if desired
  private static final String PASSWORD_PATTERN = "^(?=.*\\d)(?=.*[a-z])[a-z0-9]*$";

  private Pattern pattern;

  public PasswordCharactersValidator() {
    pattern = Pattern.compile(PASSWORD_PATTERN);
  }

  @Override
  public boolean isValid(final String password) {

    if (StringUtils.isBlank(password)) {
      return false;
    }

    Matcher matcher = pattern.matcher(password);
    return matcher.matches();
  }
}
