package com.rowellbelen.demo.validator;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

/**
 * @author Rowell Belen
 */
@Component
public class PasswordLengthValidator implements Validator {

  // These could be made configurable via properties, if desired
  private static final int PASSWORD_MIN_LENGTH = 5;
  private static final int PASSWORD_MAX_LENGTH = 12;

  @Override
  public boolean isValid(final String password) {
    if (StringUtils.isBlank(password)
        || password.length() < PASSWORD_MIN_LENGTH
        || password.length() > PASSWORD_MAX_LENGTH) {
      return false;
    }

    return true;
  }
}
