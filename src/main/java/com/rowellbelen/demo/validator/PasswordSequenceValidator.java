package com.rowellbelen.demo.validator;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

/**
 * @author Rowell Belen
 */
@Component
public class PasswordSequenceValidator implements Validator {

  @Override
  public boolean isValid(String password) {

    if (StringUtils.isBlank(password)) {
      return false;
    }

    // Not the most efficient algorithm but acceptable due to password length constraint
    for (int i = 0; i < password.length(); i++) {
      // j = 2 ( simple optimization by ignoring single-letter substring )
      for (int j = 2; j <= password.length() - i; j++) {
        String substring = password.substring(i, i + j);

        // if substring occurs more than once, we may have a repeating sequence
        if (StringUtils.countMatches(password, substring) > 1) {

          // check next substring of equal length
          int startIdx = i + j;
          int endIdx = startIdx + substring.length();
          String nextSequence = StringUtils.substring(password, startIdx, endIdx);
          if (substring.equals(nextSequence)) {
            return false; // exit as soon as first repeating sequence found
          }
        }
      }
    }

    return true;
  }
}
