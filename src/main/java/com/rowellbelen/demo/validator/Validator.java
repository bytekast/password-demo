package com.rowellbelen.demo.validator;

/**
 * @author Rowell Belen
 */
public interface Validator {
  boolean isValid(String password);
}
