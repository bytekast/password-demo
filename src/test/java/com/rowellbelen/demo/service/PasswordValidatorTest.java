package com.rowellbelen.demo.service;

import com.rowellbelen.demo.app.DemoApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author Rowell Belen
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = DemoApplication.class)
public class PasswordValidatorTest {

  @Autowired
  private PasswordValidator validator;

  @Test
  public void testIsValid() throws Exception {

    assertFalse(validator.isValid(null));
    assertFalse(validator.isValid(""));
    assertFalse(validator.isValid("\t\r"));
    assertFalse(validator.isValid("pas1"));
    assertFalse(validator.isValid("pasas1"));
    assertFalse(validator.isValid("Rowell"));
    assertFalse(validator.isValid("123456"));
    assertFalse(validator.isValid("ROWELL1"));

    assertTrue(validator.isValid("pa5sword1"));
    assertTrue(validator.isValid("password123"));
  }
}
