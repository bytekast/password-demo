package com.rowellbelen.demo.validator;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author Rowell Belen
 */
public class PasswordCharactersValidatorTest {

  private Validator validator;

  @Before
  public void setup() {
    validator = new PasswordCharactersValidator();
  }

  @Test
  public void testIsValid() throws Exception {
    assertFalse(validator.isValid(null));
    assertFalse(validator.isValid(""));
    assertFalse(validator.isValid("\t\r"));
    assertFalse(validator.isValid("Password"));
    assertFalse(validator.isValid("123456"));
    assertFalse(validator.isValid("PASSWORD1"));

    assertTrue(validator.isValid("password1"));
    assertTrue(validator.isValid("pa5sword1"));
  }
}
