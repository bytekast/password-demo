package com.rowellbelen.demo.validator;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author Rowell Belen
 */
public class PasswordLengthValidatorTest {

  private Validator validator;

  @Before
  public void setup() {
    validator = new PasswordLengthValidator();
  }

  @Test
  public void testIsValid() throws Exception {
    assertFalse(validator.isValid(null));
    assertFalse(validator.isValid(""));
    assertFalse(validator.isValid("\t\r"));
    assertFalse(validator.isValid("P"));
    assertFalse(validator.isValid("Pass"));
    assertFalse(validator.isValid("PasswordPassword"));

    assertTrue(validator.isValid("Password"));
    assertTrue(validator.isValid("1234567890pd"));
    assertTrue(validator.isValid("Password123"));
  }
}
