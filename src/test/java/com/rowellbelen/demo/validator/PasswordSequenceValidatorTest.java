package com.rowellbelen.demo.validator;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author Rowell Belen
 */
public class PasswordSequenceValidatorTest {

  private Validator validator;

  @Before
  public void setup() {
    validator = new PasswordSequenceValidator();
  }

  @Test
  public void testIsValid() throws Exception {
    assertFalse(validator.isValid(null));
    assertFalse(validator.isValid(""));
    assertFalse(validator.isValid("passpassword11"));
    assertFalse(validator.isValid("passpassword11word11"));
    assertFalse(validator.isValid("passpasassword11word11"));

    assertTrue(validator.isValid("55pa55word"));
    assertTrue(validator.isValid("pa55word"));
    assertTrue(validator.isValid("ppasword"));
    assertTrue(validator.isValid("paswordd"));
    assertTrue(validator.isValid("password"));
    assertTrue(validator.isValid("pas5w0rd"));
    assertTrue(validator.isValid("paspsword11"));
  }
}
